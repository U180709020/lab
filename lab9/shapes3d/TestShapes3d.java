package shapes3d;

public class TestShapes3d {
    public static void main(String[] args) {
        Cylinder cy = new Cylinder(5,10);
        System.out.println("Cylinder volume = " + cy.volume());
        System.out.println("Cylinder area = " + cy.area());
        //System.out.println(cy.toString());
        Cube cb = new Cube(5);
        System.out.println(cb);

    }
}
