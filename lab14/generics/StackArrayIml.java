package generics;

import java.util.ArrayList;
import java.util.List;

public class StackArrayIml<T> implements Stack<T> {
    private ArrayList<T> stack = new ArrayList<T>();
    @Override
    public void push(T Item) {
        stack.add(0,Item);

    }

    @Override
    public T pop() {
      return stack.remove(0);
    }

    @Override
    public boolean empty() {
        return stack.size()==0;
    }

    @Override
    public List<T> toList() {
        return stack;
    }

}
