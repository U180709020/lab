public class TestRectangle {

    public static void main(String[] args) {
        Point topleft = new Point(10,10);
        Rectangle rect = new Rectangle(5,6,topleft);
        System.out.println("Rectangle Area = " + rect.area() +" "+ "Rectangle Perimeter = " + rect.perimeter());
        Point[] corners = rect.corners();
        for (int i = 0; i <corners.length ; i++) {
            System.out.println("Corner " + i + " at x = "+corners[i].xCoord+ " at y = "+corners[i].yCoord);
        }

    }
}
