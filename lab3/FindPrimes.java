public class FindPrimes {
    public static void main(String[] args) {
        System.out.println("Max = " + args[0]);
        // Get the number
        int max = Integer.parseInt(args[0]);

        //For each number less than the given
        for(int number = 2; number < max; number++){
            boolean isPrime = true;
            //for each divisor less than number
            int divisor = 2;
            while(divisor < number &&  isPrime) {
                //if number is divisible by divisor
                if (number % divisor == 0) {
                    //isPrime is false
                    isPrime = false;
                    //end the loop
                    break;
                }
                divisor++;
            }


            //if the number is prime
            if(isPrime)
                //print the number
                System.out.print(number + ",");


        }



    }























}
