package demo;

import stack.Stack;
import stack.StackArrayIml;
import stack.StackImpl;


public class StackDemo {


    public static void main(String[] args) {
        System.out.println("Stack Implementation");
        Stack stack = new StackImpl();
        stack.push("A");
        stack.push(5);
        stack.push("Z");
        stack.push("B");
        stack.push("Hello");

        System.out.println("Stack is empty : " + stack.empty());

        while (!stack.empty()){
            System.out.println(stack.pop());
        }

        System.out.println("StackArray Implementation");
        Stack stack1 = new StackArrayIml();
        stack1.push("A");
        stack1.push(5);
        stack1.push("Z");
        stack1.push("B");
        stack1.push("Hello");

        System.out.println("Stack is empty : " + stack1.empty());

        while (!stack1.empty()){
            System.out.println(stack1.pop());
        }

    }
}
